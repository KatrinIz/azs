import xmlrpc.client

import aiohttp
import asyncio
import time

url_proxy = "http://localhost:8000"
server = xmlrpc.client.ServerProxy(url_proxy)
azss = [("gazprom", ['station']) , ("tatneft", [])]
# 1) simple requests questions

t1 = time.time()
def requestAzs(data, server):
    azs_type, azs_params = data
    resp = server.getAzs(azs_type, azs_params)
    return resp

resp = requestAzs(("gazprom", ['station']), server)
print(resp)
t2 = time.time() 
print (f"requests method was spent {t2-t1} seconds")




