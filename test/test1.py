import asyncio
import time

async def say_after(delay, what):
    await asyncio.sleep(delay)
    print(what)

async def main():
    t1 = time.time()

    await say_after(1, 'hello')
    await say_after(2, 'world')
    t2= time.time()
    print ("t2-t1 = ", t2-t1)


#loop = asyncio.get_event_loop()
# Blocking call which returns when the main() coroutine is done
#loop.run_until_complete(main())
#loop.close()

loop2 = asyncio.get_event_loop()
# Ожидание завершения обоих операций должно занять около 2х секунд

t3 = time.time()
loop2.run_until_complete(asyncio.gather(
    say_after(1, 'hello'),
    say_after(2, 'world')
))
t4 = time.time()
print ("t3-t4 = ", t4-t3)
loop2.close()
