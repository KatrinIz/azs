import aiohttp
import asyncio
import time

async def azs_info(url_proxy, url, params):
        async with aiohttp.ClientSession(url_proxy) as session:
            async with session.get(url, params=params) as resp:
                res = await resp.json()
                print(res)
                # if resp.status >= 200 and resp.status < 300:
                #     # return  {"status": "ok", "data": await resp.json()}
                #     print(url, params,  "ok")
                # else:
                #     print(url, params,  "break")

# async def create_session(url_proxy):
#      return aiohttp.ClientSession(url_proxy)

apis = [
    ("/bulk_azs", {"azs": ["tatneft", "gazprom"]}),
    ("/brand_list", {}),
    ("/fuel_list", {}),
    ("/azs_list", {"brand": ["tatneft"]}),
    ("/azs_info", {"id": []}),
    ("/price_range", {"brand": [], "fuel": []}),
    ("/bulk_azs", {"azs": ["tatneft"]}),
    ("/bulk_azs", {"azs": ["gazprom"]}),
]

url_proxy = "http://0.0.0.0:8080"
t1 = time.time()
# session = aiohttp.ClientSession(url_proxy)

async def main():
    await asyncio.gather(
    # azs_info(url_proxy, apis[7][0], apis[7][1]),
    azs_info(url_proxy, apis[6][0], apis[6][1])
    )
asyncio.run(main())
# loop = asyncio.get_event_loop()
# #отладить слудующий порядок обращений, должно быть: \
# # отправили запрос газпром, отправили запрос татнефть,\
# # через 3 секунды вернулся результат татнефть, ещё через 2 газпром
# loop.run_until_complete(asyncio.gather(
#     azs_info(session, apis[7][0], apis[7][1]),
#     azs_info(session, apis[6][0], apis[6][1])
#     )
# )
# # session.close() не работает..(
# loop.run_until_complete(asyncio.sleep(0))
# loop.close()

t2= time.time()
print("time = ", t2-t1 )