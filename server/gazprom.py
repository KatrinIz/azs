cookies = {}

headers = {
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'Connection': 'keep-alive',
    'Content-Type': 'application/json;charset=UTF-8',
    'User-Agent': 'test',
}

json_data = {
    'open': False,
    'wash': False,
    'AZSShopTypeID': False,
    'services': {
    },
}

url = 'https://gpnbonus.ru/api/stations/list'
params=['car', 'payment', 'person', 'station']

if __name__ == "__main__":
    import requests
    json_data_test = {
    'open': False,
    'wash': False,
    'AZSShopTypeID': False,
    'services': {'station':{}}
}
    response = requests.post(url, cookies=cookies, headers=headers, json=json_data_test)
    print(response.json())
