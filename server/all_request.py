from xmlrpc.server import SimpleXMLRPCServer
import requests
import sys

import aiohttp
import asyncio

import gazprom 
import lucoil
import tatneft
import urllib.parse

from xmlrpc.server import SimpleXMLRPCServer

def getGazprom(params=[]):

        for param in params:
            if param in gazprom.params:
                gazprom.json_data['services'][param] = {}
        try:
            response = requests.post(
                gazprom.url, 
                cookies=gazprom.cookies, 
                headers=gazprom.headers, 
                json=gazprom.json_data
                )    
            resp = response.json()
            resp["status"]=True
            return resp
        except: 
            return {"status": False}


def getLucoil(params=[]):
    """No work api"""
    try:
        response = requests.get(lucoil.url, headers=lucoil.headers)
        resp = response.json()
    except:
        return {"status": False}


def getTatneft(params=[]):
    try: 
        response = requests.get(tatneft.url, headers=tatneft.headers)
        resp = response.json()
        if "data" in resp:
            res = resp["data"]
            res["status"] =True
    except:
        return {"status": False}
 
azss = {
    "gazprom": getGazprom,
    "tatneft": getTatneft
}

def getAzs(azs_type, azs_params):
    return azss[azs_type](azs_params)


server = SimpleXMLRPCServer(("localhost", 8000), allow_none=True)
print('Serving XML-RPC on localhost port 8000')
server.register_function(getAzs,"getAzs")
try:
    server.serve_forever()
except KeyboardInterrupt:
    print("\nKeyboard interrupt received, exiting.")
    sys.exit(0)



