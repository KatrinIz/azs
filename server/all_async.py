import time

from aiohttp import web,  ClientSession
import asyncio

import gazprom 
import lucoil
import tatneft

# https://docs.aiohttp.org/en/v3.8.4/client_quickstart.html

async def post_gazprom(session):
    params=['car', 'payment', 'person', 'station']
    for param in params:
        if param in gazprom.params:
            gazprom.json_data['services'][param] = {}
    
    async with session.post(gazprom.url, 
                            headers=gazprom.headers,
                            cookies=gazprom.cookies,
                            json=gazprom.json_data) as resp:
        res = await resp.json(content_type='text/html')
        print("gazprom ready, sleep 5")
        await asyncio.sleep(5)
        return res 


async def get_tatneft(session):
    async with session.get(tatneft.url) as resp:
        res = await resp.json()
        print("tatneft ready, sleep 3")
        await asyncio.sleep(3)
        return res 

dic_azs = {
    "gazprom": post_gazprom,
    "tatneft": get_tatneft
}

dAzs_list = {
    "gazprom": "Газпром",
    "tatneft": "ТатНефть",
    "lucoil": "Лукойл"
}
fuels = ["АИ-80" "А-76", "Н-80", "АИ-92", "АИ-95", "АИ-95+", "АИ-98", "АИ-100", "ДТ-Е", "ДТ-З", "ТС-1"]


async def bulk_azs(request):
    params = request.query.getall("azs") #['gazprom', 'tatneft']
    async with ClientSession() as session:
        tasks = []
        for azs_item in params:
            print (azs_item)
            tasks.append(asyncio.ensure_future(dic_azs[azs_item](session)))

        results = await asyncio.gather(*tasks)
        resp = {"azs": results, "status":"ok", "params":params}
        return web.json_response(resp)
    

async def brand_list(request):
    # "ТатНефть", "Лукойл", "Газпром"
    data = [(key, dAzs_list[key]) for key in dAzs_list]
    return web.json_response({"data": data})
    

async def fuel_list(request):
    # "АИ-80" "А-76", "Н-80" - sinonims
    return web.json_response({"data": fuels})
    

async def azs_list(request):
    # список всех азс(возвращаем список объектов {id: ..., brand: ..., name: …, coord:[long, lat]}), если brand не указан - выводим все азс
    pass
    

async def azs_info(request):
    # /azs_info/:id - получение информации об отдельной азс по её id
    pass
    

async def price_range(request):
    # /price_range?brand=...&fuel=... - диапозон цен на товливо
    pass

app = web.Application()
app.add_routes([web.get('/bulk_azs', bulk_azs)])
app.add_routes([web.get("/brand_list", brand_list)])
app.add_routes([web.get("/fuel_list", fuel_list)])
app.add_routes([web.get("/azs_list", azs_list)])
app.add_routes([web.get("/azs_info", azs_info)])
app.add_routes([web.get("/price_range", price_range)])
web.run_app(app)
